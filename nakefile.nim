import nake
import os

var RELEASE_TYPE = ""

task "default", "Build daemon and cli-tool":
  runTask("daemon")
  runTask("tools")

task "release", "Build daemon and cli-tool in release mode":
  RELEASE_TYPE = "-d:release"
  runTask("daemon")
  runTask("tools")

task "tools", "Build only the tools":
  createDir("bin")
  direShell(nimExe, "c", RELEASE_TYPE, "src"/"cli-tool"/"main.nim")

task "daemon", "Build only the daemon":
  createDir("bin")
  direShell(nimExe, "c", RELEASE_TYPE, "src"/"daemon"/"main.nim")

task "clean", "Clean build files":
  removeFile("bin"/"elogd")
  removeFile("bin"/"elog-cli")

task "dist-clean", "Clean build files and purge intermediate files":
  runTask("clean")
  removeDir("bin")
  removeFile("nakefile")

