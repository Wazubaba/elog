import parsecfg
import strutils

type
  Settings = tuple
    address: string
    port: int
    maxcons: int
    dbpath: string
    rotate: int

proc get_or_default(cfg: Config, name: string, default: int): int =
  let value = cfg.getSectionValue("", name)
  if value.len() == 0:
    return default
  else:
    return value.parseInt()

proc get_or_default(cfg: Config, name: string, default: string): string =
  let value = cfg.getSectionValue("", name)
  if value.len() == 0:
    return default
  else:
    return value

proc load_settings*(path: string): Settings =
  let cfg = loadConfig(path)

  result = (
    address: cfg.get_or_default("address", "localhost"),
    port: cfg.get_or_default("port", 2080),
    maxcons: cfg.get_or_default("maxcons", 10),
    dbpath: cfg.get_or_default("dbpath", "/var/log/elog.db"),
    rotate: cfg.get_or_default("maxrotate", 3)
  )


