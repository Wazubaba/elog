import net
import message_t
import json

proc do_handshake* (client: Socket): bool =
  var
    clientMagic: string
    clientMagicSize: int
    readLen: int
    sendLen: int

    conv_version: cint = VERSION
    conv_magic: cstring = MAGIC

  # Ensure the connection is valid
  #echo "DEBUG: read client magic len"
  readLen = client.recv(clientMagicSize.addr, sizeof(cint))
  if readLen != sizeof(cint):
    echo "001 readlen is invalid length: ", readlen, "!=", sizeof(cint)
    return false
  #else: echo "001: recieved magic length of ", clientMagicSize
  
  #echo "DEBUG: read client magic string"
  readLen = client.recv(clientMagic, clientMagicSize)
  if readLen != clientMagicSize:
    echo "002 readlen is invalid length: ", readlen, "!=", clientMagicSize
    return false
  #else: echo "002: recieved magic string of ", clientMagic
  
  #echo "DEBUG: transmit magic string"
  discard client.send(conv_magic, MAGIC.len() * sizeof(cchar))

  # Send version to complete the handshake
  sendLen = client.send(conv_version.addr, sizeof(cint))
  if sendLen != sizeof(cint):
    echo "[SEND] 001 sendLen is invalid length: ", sendLen, "!=", clientMagicSize
    return false
  return true

proc get_message* (client: Socket): Message =
  var
    subsystem: string
    message: string
    payload: string

    # Containers for the various ints that get xfered
    subsystemSize: cint
    messageSize: cint
    payloadSize: cint
    severityOrd: cint

  var readlen: int
  readlen = client.recv(subsystemSize.addr, sizeof(cint))
  if not readlen == sizeof(cint):
    echo "001 readlen is invalid length: ", readlen, "!=", sizeof(cint)
    return nil
  #else: echo "001: subsystemsize is now ", subsystemSize

  readlen = client.recv(subsystem, subsystemSize)
  if not readlen == subsystemSize:
    echo "002 readlen is invalid length: ", readlen, "!=", subsystemSize
    return nil
  #else: echo "002: subsystem is now ", subsystem

  readlen = client.recv(severityOrd.addr, sizeof(cint))
  if not readlen == sizeof(cint):
    echo "003 readlen is invalid length: ", readlen, "!=", sizeof(cint)
    return nil
  #else: echo "003: severityOrd is now ", severityOrd

  readlen = client.recv(messageSize.addr, sizeof(cint))
  if not readlen == sizeof(cint):
    echo "004 readlen is invalid length: ", readlen, "!=", sizeof(cint)
    return nil
  #else: echo "004: messageSize is now ", messageSize

  readlen = client.recv(message, messageSize)
  if not readlen == messageSize:
    echo "005 readlen is invalid length: ", readlen, "!=", messageSize
    return nil
  #else: echo "005: message is now ", message

  readlen = client.recv(payloadSize.addr, sizeof(cint))
  if not readlen == sizeof(cint):
    echo "006 readlen is invalid length: ", readlen, "!=", sizeof(cint)
    return nil
  #else: echo "006: payloadSize is now ", payloadSize

  readlen = client.recv(payload, payloadSize)
  if not readlen == payloadSize:
    echo "007 readlen is invalid length: ", readlen, "!=", payloadSize
    return nil
  #else: echo "007: payload is now ", payload

#[
  echo "Done with socket reads"
  echo "subsystem len = ", subsystemSize
  echo "'", subsystem, "'"
  echo "'", message, "'"
  echo "'", payload, "'"
]#
  return new_message($subsystem, message, Severity(severityOrd), %payload)

