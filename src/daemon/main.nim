import db_sqlite
import config
import net
import os
import strformat
import ipc
import nativesockets
import msgstack
import message_t
import threadpool

import locks

# TODO Consider adding a recipt to the client to confirm whether the
# TODO message was recieved correctly or not...


# TODO Also get a timestamp implemented into the Message object...
var DBLOCK: Lock
DBLOCK.initLock()

var SHUTDOWN_CHANNEL: Channel[int]
SHUTDOWN_CHANNEL.open()

var MESSAGE_CHANNEL: Channel[Message]
MESSAGE_CHANNEL.open()

const MAX_OVERFLOW_QUEUE = 10
const MIN_OVERFLOW_QUEUE = 2

######################################################################
# Configuration Initialization
var (address, port, maxcons, dbPath, rotateDepth) =
  load_settings("etc/elog.ini")

setMaxPoolSize(maxcons)

######################################################################
# Database Initialization
## We are on a fresh start here, so rotate the previous log and initialize a
## new one.
if fileExists(dbPath):
  echo "Found previous log - rotating with maximum depth of ", rotateDepth
  let (path, fname, ext) = dbPath.splitFile()

  for suffix in countDown(rotateDepth, 1):
    let
      cur = joinPath(path, &"{fname}_{suffix}{ext}")
      dest = joinPath(path, &"{fname}_{suffix+1}{ext}")

    if suffix == rotateDepth:
      if fileExists(dest):
        echo "DEBUG: purging file that is beyond maxDepth"
        removeFile(dest)
    else:
      if fileExists(cur):
        echo "Found ", cur, ", renamed to ", dest
        moveFile(cur, dest)

  moveFile(dbPath, joinPath(path, &"{fname}_1{ext}"))

var stack = new_stack(dbPath)
if stack == nil:
  writeLine(stderr, "ERR: Failed to initialize message stack.")
  quit(1)

######################################################################
# Network Initialization
var socket = newSocket()
socket.setSockOpt(OptReusePort, true)
socket.bindAddr(Port(port), address)
socket.listen()


######################################################################
# Signal Handlers
proc shutdownhandler() {.noconv.} =
  echo "Shutdown signal recieved."
  echo "Waiting for workers to complete..."
  SHUTDOWN_CHANNEL.send(1)
  threadpool.sync()

  echo "Shutting down network interface..."
  socket.close()

  echo "Finishing remaining message processing..."
  while stack.len() > 0:
    stack.process()

  echo "All messages processed successfully, shutting down"
  stack.finalize()
  quit(0)


setControlCHook(shutdownhandler)

######################################################################
# Posix-exclusive Signal Handlers
when defined posix:
  {.hint: "Building with posix signal handlers enhancement".}
  import posix


  var FORCE_SHUTDOWN = false

  proc sigterm_handler(signo: cint) {.noconv.} =
    if signo == SIGTERM:
      if FORCE_SHUTDOWN:
        echo "Shutting down NOW. All remaining messages will be dropped!"
        SHUTDOWN_CHANNEL.send(2)
        threadpool.sync()
        stack.finalize()
        quit(0)
      else:
        FORCE_SHUTDOWN = true
        echo "Recieved SIGTERM."
        echo "Send SIGTERM again to force shutdown."
        shutdownhandler()

  proc sigusr1_handler(signo: cint) {.noconv.} =
    if signo == SIGUSR1:
      echo "DEBUG SIGNAL CAUGHT"
      stack.debug()

  proc sigusr2_handler(signo: cint) {.noconv.} =
    if signo == SIGUSR2:
      echo "DEBUG SIGNAL CAUGHT"
      echo "Number of todo items on the stack: ", stack.len()

  signal(SIGTERM, sigterm_handler)
  signal(SIGUSR1, sigusr1_handler)
  signal(SIGUSR2, sigusr2_handler)

elif defined windows:
  # Ensure windows at the very least runs the shutdown handler...
  system.addQuitProc(shutdownhandler)

######################################################################
# Worker Thread Functions
proc handle_stack(stack: MessageStack) =
  var
    msg: int
    cleanup = false
    canReadChannel: bool
    newMessage: Message
    overflow: bool # Used to ensure we don't ONLY add new messages in the
    # event of a buggy client thingy

  while true:
    (canReadChannel, msg) = SHUTDOWN_CHANNEL.tryRecv()
    if canReadChannel:
      if msg == 1: # Finish up the queue and exit
        cleanup = true
      elif msg == 2: # FORCE SHUTDOWN NOW
        break

    if stack.len() >= MAX_OVERFLOW_QUEUE and not overflow:
      echo "[Stack Handler Thread]: Flood queue reached - focusing on adding stack processing temporarilly"
      overflow = true

    if stack.len() <= MIN_OVERFLOW_QUEUE and overflow:
      echo "[Stack Handler Thread]: Resuming normal operation"
      overflow = false

    if not overflow:
      (canReadChannel, newMessage) = MESSAGE_CHANNEL.tryRecv()
      if canReadChannel:
        stack.add(newMessage)
        #echo "there are ", MESSAGE_CHANNEL.peek(), " items remaining in the message channel"
        continue # Immediately reloop so as to recv all new things

    if cleanup and stack.len() == 0:
      break

    if stack.len() > 0:
      withLock(DBLOCK):
        echo "[Stack Handler Thread]: Process called"
        stack.process()
    sleep(10) # Sleep so as to not burn cpu cycles and io time
  
  echo "[Stack Handler Thread]: Returning"


proc handle_connection(client: Socket, chost: string) = 
  if not client.do_handshake():
    echo "[Client Handler Thread](", chost, "):Invalid connection - rejecting."
  else:
    var message = client.get_message()
    if message == nil:
      echo "[Client Handler Thread](", chost, "): Failed to interpret message from client, ignoring"
    else:
      message.host = chost
      MESSAGE_CHANNEL.send(message)
#      withLock(DBLOCK):
#        stack.add(message)
#        stack.process()
  client.close()

######################################################################
# Main Body
spawn(handle_stack(stack))
var caddress: string
var client: Socket
var chost: string

while true:
  socket.acceptAddr(client, caddress)
  chost = caddress.getHostByAddr().name
  echo "Connection from ", chost, " (", caddress, ")"
  spawn(handle_connection(client, chost))

