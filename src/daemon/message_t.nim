import json
import net

const MAGIC* = "ELOG0x0525234"
const VERSION* = 1

type
  Severity* = enum
    info, warn, debug, critical, fatal, error

  Message* = ref MessageImpl
  MessageImpl = object
    host*: string
    message: string
    subsystem: string
    severity: Severity
    payload: JsonNode

proc new_message*(message, subsystem: string, severity: Severity, payload: JsonNode): Message =
  return Message(
    message: message,
    subsystem: subsystem,
    severity: severity,
    payload: payload)

func message*(self: Message): string {.inline.} =
  return self.message

func subsystem*(self: Message): string {.inline.} =
  return self.subsystem

func severity*(self: Message): Severity {.inline.} =
  return self.severity

func payload*(self: Message): JsonNode {.inline.} =
  return self.payload

proc `$`*(self: Message): string =
  result = "\n\tsubsystem: " & self.subsystem
  result = result & "\n\tseverity: " & $self.severity
  result = result & "\n\tmessage: " & self.message
  result = result & "\n\tpayload: " & $self.payload
