
#[
  This module implements elogd's message stack. It exists to decouple
  the networking layer from the database io layer. Unfortunately, due
  to the nature of sqlite3, we can only use a single worker thread for
  io.
]#

import message_t
import db_sqlite
import json

type
  # Using an object reference so as to make passing to a signal later easier...
  MessageStack* = ref MessageStackImpl
  MessageStackImpl = object
    todolist: seq[Message]
    db: DbConn


proc new_stack*(dbpath: string): MessageStack =
  result = MessageStack()
  result.todolist = newSeq[Message]()
  result.db = open(dbPath, "", "", "")


proc finalize*(self: MessageStack) =
  self.db.close()


func len*(self: MessageStack): int =
  return self.todolist.len()


proc process*(self: MessageStack) =
  let new_message = self.todolist.pop()
  self.db.exec(sql("""CREATE TABLE if not exists ? (
    Id INTEGER PRIMARY KEY,
    Subsystem TEXT NOT NULL,
    Severity INTEGER,
    Message TEXT,
    Payload TEXT)"""), new_message.host)

  echo "DEBUG: [msgstack.nim](45): Injecting values into table ", new_message.host, ":"
  echo "\tSubsystem: ", new_message.subsystem
  echo "\tSeverity: ", new_message.severity
  echo "\tMessage: ", new_message.message
  echo "\tPayload: ", new_message.payload
  echo "END OF DEBUG"

  self.db.exec(sql"INSERT INTO ? (Subsystem, Severity, Message, Payload) VALUES (?, ?, ?, ?)",
  new_message.host, new_message.subsystem, ord(new_message.severity),
  new_message.message, $new_message.payload)


proc add*(self: MessageStack, message: Message) =
  self.todolist.add(message)

proc debug*(self: MessageStack) =
  for item in self.db.fastRows(sql"SELECT * FROM localhost"):
    echo item

